(function() {

    'use strict';

    angular
    .module('app')
    .factory('api', apiFactory);

    apiFactory.$inject = ['$http'];

    function apiFactory($http) {

        return {
            getCats : getCats,
            sortCats : sortCats
        };

        function getCats(url) {
            return $http.jsonp(url)
            .success(function(data) {
                return data;
            });
        }

        function sortCats(data) {

            var femaleOwner = [],
                maleOwner = [],
                cats = {};

			for(var i = 0; i < data.length; i++) {
				if(angular.isArray(data[i].pets)) {
					if(data[i].gender == 'Female') {
						for(var j = 0; j < data[i].pets.length; j++) {
							if(data[i].pets[j].type == 'Cat') {
								femaleOwner.push(data[i].pets[j].name);
							}
						}
					} else {
						for(var j = 0; j < data[i].pets.length; j++) {
							if(data[i].pets[j].type == 'Cat') {
								maleOwner.push(data[i].pets[j].name);
							}
						}
					}
				}
			}
            cats = { 'male': maleOwner, 'female': femaleOwner };
            return cats;     
        }

    }

})();