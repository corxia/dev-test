(function() {

	'use strict';

	angular
	.module('app')
	.controller('CatsController', CatsController);

	CatsController.$inject = ['api'];

	function CatsController(api) {

		var vm = this;
			vm.femaleOwner = [];
			vm.maleOwner = [];

        api.getCats('http://agl-developer-test.azurewebsites.net/people.json?callback=JSON_CALLBACK').then(function(response) {
			vm.data = api.sortCats(response.data);
			vm.femaleOwner = vm.data.female;
			vm.maleOwner = vm.data.male;
		});
        
	
	}
	
}());