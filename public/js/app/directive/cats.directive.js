(function() {

    'user strict';

    angular
    .module('cats', [])
    .directive('cats', cats);

    cats.$inject = ['$http'];

    function cats($http, api) {
        var directive = {
            restrict: '',
            templateUrl: 'templates/cats.html',
            scope: {
                max: '='
            },
            controller: 'CatsController',
            controllerAs: 'vm',
            bindToController: true,
            link: linkFunc
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {

        }
    }


}());